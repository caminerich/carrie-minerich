"""
Purpose: Exports the NICC Large Incident(s) for Canada to GDB.
"""

import sys, os, shutil, codecs, re, optparse
#import arcpy
import csv, urllib2, datetime, zipfile, glob, chardet
#from dbfpy import dbf
from pprint import pprint 

#arcpy.env.overwriteOutput = 1

#Download raw file from url, rename in wrkdir as ca_province.extension and is hereon referred to as source
def DownloadCanada(regionExport):
    #dbffiles = {'bc':'prot_current_fire_points.dbf','sk':'fires2date.dbf'}
    print("Downloading Canada Files %s" % regionExport.fieldsToExtract)

    RawCanadaFile = os.path.join(wrkdir, 'ca_'+abbreviation+'.'+extension)

    SourceCanadaURL = urllib2.urlopen(url)
    CanadaSourceData = SourceCanadaURL.read()

    #.csv and .txt files only
    with open(RawCanadaFile, 'wb') as file:
        file.write(CanadaSourceData)

    '''if ext == 'zip':
        dext = os.path.splitext(os.path.basename(ofile))[1]
        oext = os.path.splitext(dbffiles[prov])[1]

        with zipfile.ZipFile(ofile) as z:
            with open(ofile.replace(dext,oext), 'wb') as f:
                f.write(z.read(dbffiles[prov]))'''


#this function converts files to UTF8
def Converter(regionExport):
    print("Convert encoding UTF 16 to UTF 8 %s" % regionExport.fieldsToExtract)

    RawCanadaFile = os.path.join(wrkdir, 'ca_'+abbreviation+'.'+extension)
    if chardet.detect(RawCanadaFile)['encoding'] =='utf-8': 
        RawCanadaFile.close()
    else:
        with open(RawCanadaFile, 'rb') as RawCanada:
            contents = RawCanada.read()
            UTF8Contents = contents.decode('utf-16').encode('utf-8')
        with open(RawCanadaFile, 'w+b') as WriteCanada:
            WriteCanada.write(UTF8Contents)


#this function extracts fields we want from a source file
def FieldExtract(regionExport):           
    print("Extracting fields %s" % regionExport.fieldsToExtract)

    CanadaSource = os.path.join(wrkdir, 'ca_'+abbreviation+'.'+extension)

    with open(CanadaSource, 'rb') as CASource:
        CanadaIntermediate = open(os.path.join(wrkdir, 'ca_'+abbreviation+'_intermediate'+'.'+extension), "wb")
        writer = csv.writer(CanadaIntermediate, )
        source = csv.reader(CASource)
        next(source, None)
        
        for row in source:
            FireData = list(row[i] for i in fields)
            writer.writerow(FireData)  
        CanadaIntermediate.close()

#This function adds a province column to the intermediate file. Currently, this has an extra space between rows
def AddProvinceName(regionExport):
    #open intermediate file
    print("Add column with province name %s" % regionExport.fieldsToExtract)

    IntermediateCanadaFile = os.path.join(wrkdir, 'ca_'+abbreviation+'_intermediate'+'.'+extension)

    with open(IntermediateCanadaFile, 'r') as Canada:
        reader = csv.reader(Canada)
        all = []
        row = next(reader)

        for row in reader:
            row.insert(0, abbreviation)
            all.append(row)

    with open(IntermediateCanadaFile, 'w') as CAIntermediate:
        writer = csv.writer(CAIntermediate)    
        writer.writerows(all)    

def Unzip(regionExport):
    print("Unzipping data from %s into %s" % (regionExport.exportUrl,regionExport.sourceFilePath))
    #BC appears to be zip file, need to confirm

def ConvertFromDBF(regionExport):
    print("  Converting From DBF data from %s into %s" % (regionExport.exportUrl,regionExport.sourceFilePath))
    #BC has a DBF format, not seen elsewhere

def ConvertFromText(regionExport):
    print("  Converting From Text from %s into %s" % (regionExport.exportUrl,regionExport.sourceFilePath))
    #MB will have a txt or possibly a csv format

#finally, take the results from all exports and do the final processing
def ImportAllResultsToGeoDatabase(allExports):
    for export in allExports:
        print("Appending %s to master incidents file" % export.finalFilePath)

    print("Ready to import master incidents file into GDB")


'''
def ExportCanadaGDB(lg_fire_ascii,lg_fire_shp,lg_fire_fc):
    #500 hectare threshold
    #check if staged ascii has data
    #get lg_fire_ascii row count, index starting at 0, return as integer. if no integer, no path exists
    #getcount returns file lg_fire_Ascii; getoutput returns # of rows and int returns the data value we want. So, if int less than 1, no files and no fires
    if int(arcpy.GetCount_management(lg_fire_ascii).getOutput(0)) < 1 or not os.path.exists(lg_fire_ascii): 
    #os.path.exist: to even check for fires, file must exist. if file does not exist, there are no large fires. 
    
        print('no large fires exist at the moment...') #this prints after reformat

        #if no published records, then purge the previous fc data
        arcpy.DeleteRows_management(lg_fire_shp)
        arcpy.DeleteRows_management(lg_fire_fc)

    else:
        #lat,long = ('latitude','longitude') if region == 'conus' else ('Latitude','Longitude')
        lat,long = ('Latitude','Longitude') 
        #set ascii data variable if we're handling canada

        ascii_canada = []
        canada_vals = []
        header = ['Province','FireID','Hectares','Longitude','Latitude']
        #this is the export txt file for the final stage of process. This variable was named incidents but changed to lg_fire_ascii
        lg_fire_ascii = r'C:\Sandbox\canada\incidents_canada.txt' #r'\\166.2.126.229\d$\projects\large_incident\output\working\incidents_canada.txt'

        #it appears this section is opening the lg_fire_ascii file, reading        
        with open(lg_fire_ascii, 'rb') as f: #open lg_fire_ascii as read binary and label each item in lg_fire_Ascii as f
            #ca_fires = csv.reader(f) #define ca_fires as reading each line of lg_fire_Ascii csv file...currently lg_fire_Ascii is txt file...
            canada_vals = list(list(rec) for rec in csv.reader(f, delimiter=',')) #populating canada_vals array as a list 
            ascii_canada.append(canada_vals) #adding canada_vals list to ascii_canada array

        with open(incidents, "wb") as output: #incidents is the path to a txt file, opening as write and defining each line of incident as output.
            writer = csv.writer(output) #define writer as writing each line of incidents as output 
            writer.writerow(header) #write header to the csv file

            for val in canada_vals: #writing each value in canada_vals list to the incidents txt that had header written in last section
                writer.writerow(val) 

            #lg_fire_ascii = incidents #why? why not keep incidents throughout function?

        #this arcpy section will need flushing out

        #create inline event layer and copy to output
        #MakeXY: create new point feature layer. temp and needs to be exported using copy features    
        #MakeXY: (table (table view), in_x_field (Field), in_y_field (Field), out_layer (Feature layer), spatial_reference)  
        arcpy.MakeXYEventLayer_management(lg_fire_ascii, long, lat, 'lg_fire', 4269) 
        #in_features, out_feature_class
        arcpy.CopyFeatures_management('lg_fire', lg_fire_shp)

        #add custom fields and calculate values
        #addfield adds a new field to a table or table of FC
        #AddField(in_table, field_name, field_type)
        arcpy.AddField_management(lg_fire_shp, 'MapID', 'Short')
        #calc values of field. (in_table, field updated with new calc, SQL expression, code type)
        arcpy.CalculateField_management(lg_fire_shp, "MapID", "!FID! + 1", "PYTHON_9.3") #python 9.3 is standard python forat for geoprocessor methods

        #Add Acres and covert hectares to acres
        #AddField(in_table, field_name, field_type)
        arcpy.AddField_management(lg_fire_shp, "Acres", 'Long')
        #calc values of field. (in_table, field updated with new calc, SQL expression with hectare conversion, code type)
        arcpy.CalculateField_management(lg_fire_shp, "Acres", "!Hectares! * 2.47105", "PYTHON_9.3")
        
        #create output copy with updated field values
        #copy features: in_features, out_feature_class
        arcpy.CopyFeatures_management(lg_fire_shp, lg_fire_fc)

    print('export_nicc_canada complete')'''

'''def commandArgs(argv):
    parser = optparse.OptionParser(description=' ')
    parser.add_option('--prov', dest='prov', help='Product Canada provience to export... (i.e. AB, BC, etc.')

    (option, args) = parser.parse_args(argv)

    prov = []

    if option.prov:
        prov = ['ab']
        print('processing canada large fires')
    else:
        print('no prov') #debug, this is printing instead of processing canada large fires

    return (option.prov)'''

class regionExport():
     #shared configuration goes here, these items are intended to be fixed and not change
    baseWorkingDirectory = r"C:\Python\Sandbox"
    sourceFileSuffix = "_source"
    intermediateFileSuffix = "_intermediate"
    finalFileSuffix = "_final"

    #set up instance data members, these are the values that we'll populate for each export region
    countryName = ""
    countryAbbreviation = ""
    regionName = ""
    regionAbbreviation = ""
    
    exportUrl = ""
    operations = []
    fieldsToExtract = []

    #calculated members here, these are values that we'll calulate based on our fixed configuration in conjunction with the data from each instance
    exportExtenion = ""
    sourceFilePath = ""
    intermediateFilePath = ""
    finalFilePath = ""

    #"constructor function" for setup, this is the function that is run every time we create a new RegionExport to help "construct" it
    def __init__(self, exportUrl, countryName, countryAbbreviation, regionName, regionAbbreviation, operations, fieldsToExtract = []):
        self.exportUrl = exportUrl

        self.countryName = countryName
        self.countryAbbreviation = countryAbbreviation

        self.regionName = regionName
        self.regionAbbreviation = regionAbbreviation
        
        #these are the functions outlines above and each export will define which operations need to be applied
        self.operations = operations
        self.fieldsToExtract = fieldsToExtract

        #figure out calculated members
        _, self.exportExtenion = os.path.splitext(exportUrl)
        self.sourceFilePath = os.path.join(self.baseWorkingDirectory, self.countryAbbreviation + "_" + self.regionAbbreviation + self.sourceFileSuffix + self.exportExtenion)
        self.intermediateFilePath = os.path.join(self.baseWorkingDirectory, self.countryAbbreviation + "_" + self.regionAbbreviation + self.intermediateFileSuffix + ".csv")
        self.finalFilePath = os.path.join(self.baseWorkingDirectory, self.countryAbbreviation + "_" + self.regionAbbreviation + self.finalFileSuffix + ".csv")

    #self in python is passed to instance methods as the current intance of the object being operated on.
    def doExport(self):
        print("=====Currently exporting data for %s, %s from %s:=====" % (self.regionName, self.countryName, self.exportUrl))
        print("===BEG Export Configuration===")
        pprint(vars(self))
        print("===END Export Configuration===")

        #call each operation in the list of operations, passing in the current region
        for operation in self.operations:
            operation(self)

        print("=====Export complete for %s, %s=====" % (self.regionName, self.countryName))
        print("")



#set up and configure all of our exports here:
#Format: RegionExport(exportUrl, countryName, countryAbbreviation, regionName, regionAbbreviation, operations, fields)

exports = [
    RegionExport('http://wildfire.alberta.ca/reports/activedd.csv', "Canada", "ca", "Alberta", "ab", [Download, ConvertFromUTF16ToUTF8, ExtractFields, AddRegionColumn, AddHeaders], [4,16,9,8]),
    RegionExport('https://pub.data.gov.bc.ca/datasets/2790e3f7-6395-4230-8545-04efb5a18800/prot_current_fire_points.zip'), "Canada", "ca", "British Columnbia", "bc", [Download, Unzip, ConvertFromDBF, ExtractFields, AddRegionColumn, AddHeaders], [2,3,10,11]),
    RegionExport('https://www.gov.mb.ca/sd/fire/Fire-Statistic/2019/MBFIRE2019.txt'), "Canada", "ca", "Manitoba", "mb", [Download, ConvertFromText, AddHeaders], [3,15,6,8]),
]

for export in exports:
    export.doExport()

#now that exports are complete
ImportAllResultsToGeoDatabase(exports)      
   