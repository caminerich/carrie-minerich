
import subprocess, os
import timestamper #for calling external executables. 27 subprocess.call and 37 is subprocess.run
#Make the Postgres bin directory current workdir
#work_dir = r'C:\Program Files\PostgreSQL\10\bin' 

def do_backup(db_name, path):
    print 'starting backup of %s' % db_name

    backup_filename = os.path.join(path, '%s.sql' % timestamper.get_filename('%s_db_backup'% db_name)) #timestamper is module name and get_filename is function being called.
    #passing db_name into "db_name"_db_backup "timestamp" function from timestamper module. 
    print 'backing up as %s' % backup_filename
    subprocess.call([r'C:\Program Files\PostgreSQL\10\bin\pg_dump.exe', '-d', db_name, '-U', 'postgres', '-f', backup_filename]) #cmd line of pgdump cmd to run backup. 
    #remember to add password as a config file in user\carrieminerich folder AND/OR add password on global environment variables. 
    #C:\Users\carrieminerich\AppData\Roaming\postgresql\pgpass.conf 
    print 'backup finished'

if __name__=="__main__":
    print 'running as script'
    do_backup('afm_sample', r'C:\path') #for debugging, hardcode db name and path here
else:
    print 'running as module'
    print __name__    