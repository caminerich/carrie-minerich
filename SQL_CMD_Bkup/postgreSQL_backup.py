'''
conduct regular postgres backups through command line -done
run python script in system scheduler to create regular backups
account for backup file retention and delete old backups
'''

'''
These backups will need to be restored using command line.
Remember to add config file with password to machine with db on it. Update port on config file when pg11 goes live. 
'''
#import librarys
import dbcleanup, pgbackup

pgbackup.do_backup('afm', r'C:\Python') #pass db name in do_backup function of pgbackup module
dbcleanup.do_cleanup(r'C:\Python') #ID path here to be passed through subsequent functions, include age in argument if different thatn 365 days till deletion