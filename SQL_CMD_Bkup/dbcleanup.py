#delete previous backups or delete all files except from the 1st of the month so there is a rolling backup. 
#Allow for overwrite of each calendar month so there is a rolling 12 backups totaling a year
#look at os module
import os, sys
import datetime, time

#create list of available sql files in the backup path folder
def list_bkups(path):
    #path = r'C:\Python' #do not need this as path is passed in do_cleanup function when list_bkups is called. not id'ing the path makes the function more generic since 
    #we id the path in the do_cleanup function when list_bkups is called. 
    bkup_dir = os.listdir(path)
    
    ReturnValue = []

    for rawfile in bkup_dir:
        if rawfile.endswith("sql"):
            ReturnValue.append(rawfile) 
            #print (rawfile)    
    return ReturnValue #so now all sql files are in the ReturnValue container

#list_bkups() #this is a debugging line

#this function is providing parameters to apply to a file on whether it should be deleted or not
def id_oldbackups(path,filetotest,age): #filetotest is the file name. Should this be (filetotest,path)
    ShouldDelete = False

    #path = r'C:\Python'#add this to the argument string being passed through function?

    stat = os.stat(os.path.join(path,filetotest))  #locate the file name in the path
    ctime = stat.st_ctime #this will id file creation time in unix for the given file
    #print(ctime) #debugging statement

    #convert the unix timestamp into a datetime to make it easier to use
    tstamp = datetime.datetime.fromtimestamp(ctime) #provides timestamp for the file converted to python datetime
    #print(tstamp) #debugging statement

    #assign a datetime to a variable that  is the cutoff point for the point in time that we want to delete from
    tdelta = datetime.timedelta(days=age) #change to hours=12 for debugging
   
    tcutoff = datetime.datetime.now() - tdelta #this gives me last years date.

    #write an if statement to determine if the file is older than our cutoff point
    if tstamp < tcutoff:
        ShouldDelete = True
        
    #if we should delete the file, set the Should Delete boolean to true
    return ShouldDelete #this returns the result of the function

#id_oldbackups()

#list_bkups returns fully qualified pathway so id_backups doesn't need to figure it out on its own. 
def do_cleanup(path,age=365): #age default of 365 days however postgresql_backup script will overwrite if arguement passes is different
    print('delete age of document older than %s days' % age)
    #path = r'C:\Python' #we are passing path as an argument/parameter in do_cleanup so do not need to id the path variable inside the function
    sqlfiles = list_bkups(path) #returning the results of the list_bkups fxn here as sqlfiles variable, ie a list of sql files
    for sqlfile in sqlfiles: #go through sql one at a time...loops though each file each time 
        if id_oldbackups(path,sqlfile,age): #passes each sql file through the id_oldbackup and if returns should delete, goes onto next step in if statement
            print('deleting file %s' % sqlfile)
            os.remove(os.path.join(path,sqlfile))    #removing old files as defined by id_oldfiles tdelta
        else:
            print('retaining file %s' % sqlfile)


if __name__=="__main__":
    print 'running as script'
    do_cleanup(r'C:\Python', 10) #debugging, pass path as argument and number for aged days of document to delete
else:
    print 'running as module'
    print __name__            

'''
    #timestamp module to keep files only 1 year (365-julian date)
    #pass #make a list of backups
    #figure out which backups to keep
    #remove the backups i don't want to keep
'''    