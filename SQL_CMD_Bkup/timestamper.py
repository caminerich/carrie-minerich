#need a date/time structure for the backup file name
import datetime

def get_timestamp():
    date = datetime.datetime.now() #current timestamp
    print(date.strftime("%m_%d_%y"))
    return date.strftime("%m_%d_%y") #returns current timestamp in format designated 
    #print(date)
    #return(date)

def get_filename(prefix):
    return '%s_%s' % (prefix, get_timestamp()) #prefix generalizes get filename. get filename attaches timestamp to any filename of our choosing. 
    #so will return file name, get_timestamp
    
if __name__=="__main__":
    print 'running as script'
    print get_timestamp()
    print get_filename('db-name') #place db name in here if running as a script alone
else:
    print 'running as module'
    print __name__    