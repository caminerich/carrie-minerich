import requests, json, urllib
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import psycopg2

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:postgres@localhost/iss'
db = SQLAlchemy(app)

class MyData:
    Timer = ""
    Latitude = ""
    Longitude = ""

    def __init__(self, timer, lat, long):
        
        self.Timer = timer
        self.Latitude = lat
        self.Longitude = long


def GetRemoteData():
    url = r'http://api.open-notify.org/iss-now.json'
    r = requests.get(url)
    r_dict = r.json()

    location = r_dict['iss_position']
    lat = location['latitude']
    long = location['longitude']
    timer = r_dict['timestamp']

    return MyData(timer, lat, long)


class isslocation(db.Model):
    __tablename__ = 'isslocation'

    timestamp = db.Column('timestamp', db.Integer, primary_key=True)
    latitude = db.Column('latitude', db.Numeric)
    longitude = db.Column('longitude', db.Numeric)
    
    def __init__(self, timestamp, latitude, longitude):
        self.timestamp = timestamp
        self.latitude = latitude 
        self.longitude = longitude

def SaveData(remoteData):

    issloc = isslocation(remoteData.Timer, remoteData.Latitude, remoteData.Longitude)
    db.session.add(issloc)
    db.session.commit()
    db.session.close()

    print("ISS Location complete")


#Coordinate getting and saving of remote data
def GetAndSaveData():
    currentRemoteData = GetRemoteData()
    SaveData(currentRemoteData)

if __name__ == '__main__':
    print("running as script")
    GetAndSaveData()
    
else:
    print("running as module")
    print(__name__)